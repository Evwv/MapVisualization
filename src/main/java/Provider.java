import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Provider {
    private List<MyGeoPosition> track;
    private List<MyGeoPosition> collectiveGeoPositions;
    private List<CustomRoute> routes;
    private Long id;
    private ProviderTypes connectionType;
    private String name;
    private Color geoPositionColor;
    private Color routeColor;

    Provider() {
        id = new Date().getTime();
        this.track = new ArrayList<>();
        this.collectiveGeoPositions = new ArrayList<>();
        this.routes = new ArrayList<>();
        geoPositionColor = GeoPositionUtil.getRandomColor(1);
        routeColor = GeoPositionUtil.getRandomColor((float) Math.random());
    }

    Provider(List<MyGeoPosition> track, List<CustomRoute> routes) {
        id = new Date().getTime();
        this.track = track;
        this.collectiveGeoPositions = new ArrayList<>();;
        this.routes = routes;
        geoPositionColor = GeoPositionUtil.getRandomColor(1);
        routeColor = GeoPositionUtil.getRandomColor((float) Math.random());
    }

    public List<CustomRoute> getRoutes() {
        return routes;
    }

    public void setRoutes(List<CustomRoute> routes) {
        this.routes = routes;
    }

    public boolean addRoutes(CustomRoute route) {
        if (isRouteAlreadyExists(route.getRoute())) {
            removeRoute(route.getRoute());
        }
        routes.add(route);
        return true;
    }

    public void removeRoute(List<MyGeoPosition> route) {
        if (isRouteAlreadyExists(route)) {
            setRoutes(routes
                    .stream()
                    .filter((CustomRoute items) -> {
                        return !GeoPositionUtil.checkIfRoutesEquals(items.getRoute(), route);
                    })
                    .collect(Collectors.toList())
            );
        }
    }

    public boolean isRouteAlreadyExists(List<MyGeoPosition> route) {
        return routes.stream().anyMatch((CustomRoute items) -> {
            return GeoPositionUtil.checkIfRoutesEquals(items.getRoute(), route);
        });
    }

    public List<MyGeoPosition> getTrack() {
        return track;
    }

    public List<MyGeoPosition> getAllGeoPositions() {
        return Stream.concat(track.stream(), collectiveGeoPositions.stream())
                .collect(Collectors.toList());
    }

    public void setTrack(List<MyGeoPosition> track) {
        this.track = track;
    }

    public void addGeoPositionToTrack(MyGeoPosition geoPosition) {
        geoPosition.addProviderName(name);
        this.track.add(geoPosition);
    }

    public void removeGeoPosition(MyGeoPosition geoPosition) {
        geoPosition.removeProviderName(name);
        this.track.remove(geoPosition);
        for (CustomRoute route : routes) {
            if (route.getRoute().get(0).getId().equals(geoPosition.getId()) || route.getRoute().get(1).getId().equals(geoPosition.getId())) {
                this.removeRoute(route.getRoute());
            }
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFirstLetter() {
        return name.substring(0, 1);
    }

    public void setName(String name) {
        for (MyGeoPosition myGeoPosition : track) {
            myGeoPosition.removeProviderName(this.name);
            myGeoPosition.addProviderName(name);
        }
        this.name = name;
    }

    @Override()
    public String toString() {
        return getName();
    }

    public Color getRouteColor() {
        return routeColor;
    }

    public void setRouteColor(Color routeColor) {
        this.routeColor = routeColor;
    }

    public Color getGeoPositionColor() {
        return geoPositionColor;
    }

    public void setGeoPositionColor(Color geoPositionColor) {
        this.geoPositionColor = geoPositionColor;
    }

    public List<MyGeoPosition> getCollectiveGeoPositions() {
        return collectiveGeoPositions;
    }

    public void setCollectiveGeoPositions(List<MyGeoPosition> collectiveGeoPositions) {
        this.collectiveGeoPositions = collectiveGeoPositions;
    }

    public void addCollectiveGeoPosition(MyGeoPosition myGeoPosition) {
        myGeoPosition.addProviderName(name);
        this.collectiveGeoPositions.add(myGeoPosition);
    }

    public void removeCollectiveGeoPosition(MyGeoPosition myGeoPosition) {
        myGeoPosition.removeProviderName(name);
        this.collectiveGeoPositions.remove(myGeoPosition);
        for (CustomRoute route : routes) {
            if (route.getRoute().get(0).getId().equals(myGeoPosition.getId()) || route.getRoute().get(1).getId().equals(myGeoPosition.getId())) {
                this.removeRoute(route.getRoute());
            }
        }
    }

    public boolean isGeoPositionCollective(MyGeoPosition myGeoPosition) {
        return this.collectiveGeoPositions.contains(myGeoPosition);
    }

    public ProviderTypes getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(ProviderTypes connectionType) {
        this.connectionType = connectionType;
    }
}
