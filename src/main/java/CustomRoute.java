import java.util.List;

public class CustomRoute {
    private List<MyGeoPosition> route;
    private Integer weight;

    CustomRoute (List<MyGeoPosition> route, Integer weight) {
        this.route = route;
        this.weight = weight;
    }

    public List<MyGeoPosition> getRoute() {
        return route;
    }

    public void setRoute(List<MyGeoPosition> route) {
        this.route = route;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}
