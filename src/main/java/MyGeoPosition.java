import org.jxmapviewer.viewer.GeoPosition;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyGeoPosition extends GeoPosition {
    private Long id;
    private String index;
    private List<String> providerNames;
    public MyGeoPosition(double latitude, double longitude) {
        super(latitude, longitude);
        id = new Date().getTime();
        providerNames = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return getProvidersFirstLetters() + getIndex();
    }

    public List<String> getProviderNames() {
        return providerNames;
    }

    public void setProviderNames(List<String> providerNames) {
        this.providerNames = providerNames;
    }

    public void addProviderName(String providerName) {
        this.providerNames.add(providerName);
    }

    public void removeProviderName(String providerName) {
        this.providerNames.remove(providerName);
    }

    public String getProvidersFirstLetters() {
        return providerNames.stream().map((item) -> item.substring(0,1)).reduce((result, current) -> result + current).get();
    }
}
