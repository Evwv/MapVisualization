public enum ProviderTypes {
    FIBER_OPTIC("FIBER_OPTIC"),
    MOBILE_CONNECTION("MOBILE_CONNECTION"),
    SATELLITE_CONNECTION("SATELLITE_CONNECTION"),
    COPPER_COMMUNICATION_LINES("COPPER_COMMUNICATION_LINES");

    private String expression;

    private ProviderTypes(String expression) {
        this.expression = expression;
    }

    public String getExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return expression;
    }
}
