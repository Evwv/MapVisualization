import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.painter.CompoundPainter;
import org.jxmapviewer.painter.Painter;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.WaypointPainter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;
import java.util.List;

public class CustomListener implements MouseListener {

    private JXMapViewer viewer;

    private JComboBox selectFirstWaypoint;
    private JComboBox selectSecondWaypoint;
    private JComboBox providersCombobox;
    private JComboBox geoPositionsCombobox;
    private JComboBox providerCollectiveComboBox;
    private JComboBox geoPositionsCollectiveComboBox;

    public CustomListener(
            JXMapViewer viewer,
            JComboBox selectFirstWaypoint,
            JComboBox selectSecondWaypoint,
            JComboBox providersCombobox,
            JComboBox geoPositionsCombobox,
            JComboBox providerCollectiveComboBox,
            JComboBox geoPositionsCollectiveComboBox
    ) {
        this.viewer = viewer;
        this.selectFirstWaypoint = selectFirstWaypoint;
        this.selectSecondWaypoint = selectSecondWaypoint;
        this.providersCombobox = providersCombobox;
        this.geoPositionsCombobox = geoPositionsCombobox;
        this.providerCollectiveComboBox = providerCollectiveComboBox;
        this.geoPositionsCollectiveComboBox = geoPositionsCollectiveComboBox;
    }

    public void mouseClicked(MouseEvent e) {
        Point p = e.getPoint();
        GeoPosition myGeoPosition = viewer.convertPointToGeoPosition(p);
        addGeoPoint(new MyGeoPosition(myGeoPosition.getLatitude(), myGeoPosition.getLongitude()));
    }

    WaypointPainter<MyWaypoint> waypointPainter = new WaypointPainter<MyWaypoint>();
    List<Painter<JXMapViewer>> painters = new ArrayList<Painter<JXMapViewer>>();
    Set<MyWaypoint> waypoints = new HashSet<>();

    List<Provider> providers = new ArrayList<>();
    Provider selectedProvider;

    private void addGeoPoint(MyGeoPosition geoPosition) {
        if (selectedProvider.getTrack().size() > 0) {
            int newIndex = Integer.parseInt(selectedProvider.getTrack().get(selectedProvider.getTrack().size() - 1).getIndex()) + 1;
            geoPosition.setIndex(Integer.toString(newIndex));
        } else {
            geoPosition.setIndex("0");
        }
        selectedProvider.addGeoPositionToTrack(geoPosition);
        geoPositionsCombobox.addItem(geoPosition);
        selectFirstWaypoint.addItem(geoPosition);
        selectSecondWaypoint.addItem(geoPosition);

        MyWaypoint myWaypoint = new MyWaypoint(geoPosition.toString(), selectedProvider.getGeoPositionColor(), geoPosition);
        waypoints.add(myWaypoint);
        waypointPainter.setWaypoints(waypoints);
        waypointPainter.setRenderer(new FancyWaypointRenderer());

        viewer.setOverlayPainter(waypointPainter);

        painters.add(waypointPainter);

        CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);
        viewer.setOverlayPainter(painter);

    }

    public void addProvider(ProviderTypes type) {
        Provider provider = new Provider();
        provider.setName("New Provider");
        provider.setConnectionType(type);
        selectedProvider = provider;
        providers.add(provider);
        providersCombobox.addItem(provider);
        providersCombobox.setSelectedItem(provider);
    }

    public void setProvidersName(String newName) {
        selectedProvider.setName(newName);
        printLayerByProvider(selectedProvider);
    }

    public void selectProvider(Provider provider) {
        if (provider == null) {
            return;
        }
        selectedProvider = provider;
        rerenderComboBoxes();
        printLayerByProvider(selectedProvider);
    }

    public void removeProvider(Provider provider) {
        boolean deleted = providers.remove(provider);
        ProviderTypes type = provider.getConnectionType();
        providersCombobox.removeAllItems();
        if (deleted && providers.size() > 0) {
            for (Provider singleProvider : providers) {
                providersCombobox.addItem(singleProvider);
            }
            providersCombobox.setSelectedItem(providers.get(0));
            selectProvider(providers.get(0));
        } else {
            addProvider(type);
        }

    }

    public void removeGeoPosition(MyGeoPosition myGeoPosition) {
        selectedProvider.removeGeoPosition(myGeoPosition);
        for (Provider provider : providers) {
            if (provider.isGeoPositionCollective(myGeoPosition)) {
                provider.removeCollectiveGeoPosition(myGeoPosition);
            }
        }
        rerenderComboBoxes();
        printLayerByProvider(selectedProvider);
    }

    public boolean createRoute(Long firstPointId, Long secondPointId, Integer weight) {
        if (firstPointId.equals(secondPointId)) {
            return false;
        }
        MyGeoPosition positionOne = null;
        MyGeoPosition positionTwo = null;
        for (MyGeoPosition myGeoPosition : selectedProvider.getAllGeoPositions()) {
            if (myGeoPosition.getId().equals(firstPointId)) {
                positionOne = myGeoPosition;
            }
            if (myGeoPosition.getId().equals(secondPointId)) {
                positionTwo = myGeoPosition;
            }
        }

        boolean isNewRoute = selectedProvider.addRoutes(new CustomRoute(Arrays.asList(positionOne, positionTwo), weight));

        if (isNewRoute) {
            RoutePainter routePainter = new RoutePainter(GeoPositionUtil.toGeoPositionRoute(positionOne, positionTwo), selectedProvider.getRouteColor());
            painters.add(routePainter);
            CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);
            viewer.setOverlayPainter(painter);
            return true;
        }
        return false;
    }

    public void deleteRoute(MyGeoPosition firstPoint, MyGeoPosition secondPoint) {
        selectedProvider.removeRoute(Arrays.asList(firstPoint, secondPoint));
        printLayerByProvider(selectedProvider);
    }

    private void rerenderComboBoxes() {
        geoPositionsCombobox.removeAllItems();
        selectFirstWaypoint.removeAllItems();
        selectSecondWaypoint.removeAllItems();
        for (MyGeoPosition myGeoPosition : selectedProvider.getAllGeoPositions()) {
            if (!selectedProvider.isGeoPositionCollective(myGeoPosition)) {
                geoPositionsCombobox.addItem(myGeoPosition);
            }
            selectFirstWaypoint.addItem(myGeoPosition);
            selectSecondWaypoint.addItem(myGeoPosition);
        }
    }

    private void printLayerByProvider(Provider provider) {
        painters.clear();
        waypoints.clear();
        printLayer(provider);
        CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);
        viewer.setOverlayPainter(painter);
    }

    private void printLayer(Provider provider) {

        for (CustomRoute route : provider.getRoutes()) {
            RoutePainter routePainter = new RoutePainter(GeoPositionUtil.toGeoPositionRoute(route.getRoute()), provider.getRouteColor());
            painters.add(routePainter);
        }

        for (MyGeoPosition position : provider.getAllGeoPositions()) {
            MyWaypoint myWaypoint = new MyWaypoint(position.toString(), provider.getGeoPositionColor(), position);
            waypoints.add(myWaypoint);
        }

        waypointPainter.setWaypoints(waypoints);
        waypointPainter.setRenderer(new FancyWaypointRenderer());

        viewer.setOverlayPainter(waypointPainter);

        //painters.add(routePainter);
        painters.add(waypointPainter);

    }

    public void printAllView() {
        painters.clear();
        waypoints.clear();
        for (Provider provider : providers) {
            printLayer(provider);
        }
        CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);
        viewer.setOverlayPainter(painter);
    }

    public void setProviderToCollectiveComboBox() {
        providerCollectiveComboBox.removeAllItems();
        for (Provider provider: providers) {
            if (!selectedProvider.getId().equals(provider.getId())) {
                providerCollectiveComboBox.addItem(provider);
            }
        }
    }

    public void printSelectedProviderWithCurrent(Provider provider) {
        painters.clear();
        waypoints.clear();
        printLayer(selectedProvider);
        printLayer(provider);
        CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);
        viewer.setOverlayPainter(painter);
    }

    public void printSelectedItem() {
        printLayerByProvider(selectedProvider);
    }

    public void setPositionsToCollectiveComboBox(Provider provider) {
        geoPositionsCollectiveComboBox.removeAllItems();
        for (MyGeoPosition myGeoPosition : provider.getTrack()) {
            geoPositionsCollectiveComboBox.addItem(myGeoPosition);
        }
    }

    public boolean collectiviseGeoPosition(MyGeoPosition myGeoPosition) {
        if (selectedProvider.isGeoPositionCollective(myGeoPosition)) {
            selectedProvider.removeCollectiveGeoPosition(myGeoPosition);
            rerenderComboBoxes();
            return false;
        } else {
            selectedProvider.addCollectiveGeoPosition(myGeoPosition);
            rerenderComboBoxes();
            return true;
        }
    }

    public void mouseEntered(MouseEvent e) {
        JXMapViewer button = (JXMapViewer) e.getSource();
    }

    public void mouseExited(MouseEvent e) {
        JXMapViewer button = (JXMapViewer) e.getSource();
    }

    public void mousePressed(MouseEvent e) {
        JXMapViewer viewer = (JXMapViewer) e.getSource();

    }

    public void mouseReleased(MouseEvent e) {
        JXMapViewer button = (JXMapViewer) e.getSource();
    }


    private Long generateGeoPositionsHash(Long first, Long second) {
        return first + second;
    }
}
