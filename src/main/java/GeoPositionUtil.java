import org.jxmapviewer.viewer.GeoPosition;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeoPositionUtil {
    static boolean checkIfRoutesEquals(List<MyGeoPosition> firstRoute, List<MyGeoPosition> secondRoute) {
        return (firstRoute.get(0).getId().equals(secondRoute.get(0).getId())
                        || firstRoute.get(0).getId().equals(secondRoute.get(1).getId())) &&
                (firstRoute.get(1).getId().equals(secondRoute.get(0).getId())
                        || firstRoute.get(1).getId().equals(secondRoute.get(1).getId()));
    }

    static GeoPosition toGeoPosition(MyGeoPosition myGeoPosition) {
        return new GeoPosition(myGeoPosition.getLatitude(), myGeoPosition.getLongitude());
    }

    static List<GeoPosition> toGeoPositionRoute(List<MyGeoPosition> myGeoPositionList) {
        return GeoPositionUtil.toGeoPositionRoute(myGeoPositionList.get(0), myGeoPositionList.get(1));
    }

    static List<GeoPosition> toGeoPositionRoute(MyGeoPosition firstGeoPosition, MyGeoPosition secondGeoPosition) {
        return new ArrayList<>(Arrays.asList(GeoPositionUtil.toGeoPosition(firstGeoPosition), GeoPositionUtil.toGeoPosition(secondGeoPosition)));
    }

    static Color getRandomColor(float b) {
        return Color.getHSBColor((float) Math.random(), (float) Math.random(), (float) b);
    }
}
