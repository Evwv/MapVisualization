
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.OSMTileFactoryInfo;
import org.jxmapviewer.input.CenterMapListener;
import org.jxmapviewer.input.PanKeyListener;
import org.jxmapviewer.input.PanMouseInputListener;
import org.jxmapviewer.input.ZoomMouseWheelListenerCursor;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.TileFactoryInfo;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.*;
import java.util.List;
import org.codehaus.jackson.map.ObjectMapper;


/**
 * A simple sample application that shows
 * a OSM map of Europe containing a route with waypoints
 * @author Martin Steiger
 */
public class Main
{

    public static void main(String[] args)
    {
        JXMapViewer mapViewer = new JXMapViewer();

        JFrame frame = new JFrame("Visualization");
        frame.getContentPane().add(mapViewer);
        frame.setSize(1200, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        LeftMenu leftMenu = new LeftMenu();
        CollectivePositionSelectionMenu collectivePositionSelectionMenu = new CollectivePositionSelectionMenu();
        frame.add(leftMenu.jPanel, BorderLayout.WEST);
        frame.setVisible(true);

        // Create a TileFactoryInfo for OpenStreetMap
        TileFactoryInfo info = new OSMTileFactoryInfo();
        DefaultTileFactory tileFactory = new DefaultTileFactory(info);
        mapViewer.setTileFactory(tileFactory);

        // Set the focus
        GeoPosition moscow = new GeoPosition(55.81, 37.66);
        mapViewer.setZoom(14);
        mapViewer.setAddressLocation(moscow);

        // Add interactions
        MouseInputListener mia = new PanMouseInputListener(mapViewer);
        mapViewer.addMouseListener(mia);
        mapViewer.addMouseMotionListener(mia);

        mapViewer.addMouseListener(new CenterMapListener(mapViewer));

        mapViewer.addMouseWheelListener(new ZoomMouseWheelListenerCursor(mapViewer));

        mapViewer.addKeyListener(new PanKeyListener(mapViewer));
        CustomListener customListener = new CustomListener(
                mapViewer,
                leftMenu.selectFirstWaypoint,
                leftMenu.selectSecondWaypoint,
                leftMenu.selectLayer,
                leftMenu.selectGeoPosition,
                collectivePositionSelectionMenu.providerComboBox,
                collectivePositionSelectionMenu.selectPositionComboBox
        );
        customListener.addProvider(ProviderTypes.FIBER_OPTIC);
        mapViewer.addMouseListener(customListener);

        leftMenu.createConnectionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                customListener.createRoute(
                        ((MyGeoPosition) leftMenu.selectFirstWaypoint.getSelectedItem()).getId(),
                        ((MyGeoPosition) leftMenu.selectSecondWaypoint.getSelectedItem()).getId(),
                        Integer.parseInt(leftMenu.weightField.getText())
                );
            }
        });

        leftMenu.deleteConnectionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                customListener.deleteRoute(((MyGeoPosition) leftMenu.selectFirstWaypoint.getSelectedItem()), ((MyGeoPosition) leftMenu.selectSecondWaypoint.getSelectedItem()));
            }
        });

        leftMenu.setNameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!(leftMenu.layerName.getText().length() == 0)) {
                    customListener.setProvidersName(leftMenu.layerName.getText());
                    leftMenu.layerName.setText("");
                    frame.repaint();
                }
            }
        });

        leftMenu.addLayerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                customListener.addProvider((ProviderTypes) leftMenu.providerTypeSelection.getSelectedItem());
            }
        });

        leftMenu.selectLayer.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                customListener.selectProvider(((Provider) leftMenu.selectLayer.getSelectedItem()));
                leftMenu.providerTypeSelection.setSelectedItem(((Provider) leftMenu.selectLayer.getSelectedItem()).getConnectionType());
            }
        });

        leftMenu.deleteProviderButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                customListener.removeProvider(((Provider) leftMenu.selectLayer.getSelectedItem()));
            }
        });

        leftMenu.deleteGeoPositionButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                customListener.removeGeoPosition(((MyGeoPosition) leftMenu.selectGeoPosition.getSelectedItem()));
            }
        });

        String viewAllText = "View All";
        String showSelected = "View Selected";
        final boolean[] isViewAll = {true};

        leftMenu.viewAllGraphButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (isViewAll[0]) {
                    customListener.printAllView();
                } else {
                    customListener.printSelectedItem();
                }
                leftMenu.selectLayer.setEnabled(!isViewAll[0]);
                leftMenu.addLayerButton.setEnabled(!isViewAll[0]);
                leftMenu.layerName.setEnabled(!isViewAll[0]);
                leftMenu.setNameButton.setEnabled(!isViewAll[0]);
                leftMenu.deleteProviderButton.setEnabled(!isViewAll[0]);
                leftMenu.selectGeoPosition.setEnabled(!isViewAll[0]);
                leftMenu.deleteGeoPositionButton.setEnabled(!isViewAll[0]);
                leftMenu.selectFirstWaypoint.setEnabled(!isViewAll[0]);
                leftMenu.selectSecondWaypoint.setEnabled(!isViewAll[0]);
                leftMenu.createConnectionButton.setEnabled(!isViewAll[0]);
                leftMenu.deleteConnectionButton.setEnabled(!isViewAll[0]);
                leftMenu.createCollectiveGeoPositionButton.setEnabled(!isViewAll[0]);
                isViewAll[0] = !isViewAll[0];
                leftMenu.viewAllGraphButton.setText(isViewAll[0] ? viewAllText : showSelected);
            }
        });

        leftMenu.createCollectiveGeoPositionButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                customListener.setProviderToCollectiveComboBox();

                frame.remove(leftMenu.jPanel);
                frame.add(collectivePositionSelectionMenu.jPanel, BorderLayout.WEST);
                frame.repaint();
                frame.setVisible(true);
            }
        });

        collectivePositionSelectionMenu.backButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                customListener.printSelectedItem();

                frame.remove(collectivePositionSelectionMenu.jPanel);
                frame.add(leftMenu.jPanel, BorderLayout.WEST);
                frame.repaint();
                frame.setVisible(true);
            }
        });

        collectivePositionSelectionMenu.providerComboBox.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (collectivePositionSelectionMenu.providerComboBox.getSelectedItem() != null) {
                    customListener.setPositionsToCollectiveComboBox(((Provider) collectivePositionSelectionMenu.providerComboBox.getSelectedItem()));
                    customListener.printSelectedProviderWithCurrent(((Provider) collectivePositionSelectionMenu.providerComboBox.getSelectedItem()));
                }
            }
        });

        String linkButtonText = "Link";
        String unlinkButtonText = "Unlink";

        collectivePositionSelectionMenu.selectPositionComboBox.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (collectivePositionSelectionMenu.selectPositionComboBox.getSelectedItem() != null) {
                    if (customListener.selectedProvider.isGeoPositionCollective((MyGeoPosition) collectivePositionSelectionMenu.selectPositionComboBox.getSelectedItem())) {
                        collectivePositionSelectionMenu.linkButton.setText(unlinkButtonText);
                    } else {
                        collectivePositionSelectionMenu.linkButton.setText(linkButtonText);
                    }
                }
            }
        });

        collectivePositionSelectionMenu.linkButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (collectivePositionSelectionMenu.selectPositionComboBox.getSelectedItem() != null) {
                    if (customListener.collectiviseGeoPosition((MyGeoPosition) collectivePositionSelectionMenu.selectPositionComboBox.getSelectedItem())) {
                        collectivePositionSelectionMenu.linkButton.setText(unlinkButtonText);
                    } else {
                        collectivePositionSelectionMenu.linkButton.setText(linkButtonText);
                    }
                    customListener.printSelectedProviderWithCurrent(((Provider) collectivePositionSelectionMenu.providerComboBox.getSelectedItem()));
                    frame.repaint();
                    frame.setVisible(true);
                }
            }
        });

        leftMenu.exportButton.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Specify a file to save");
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("*.json", "json"));

                int userSelection = fileChooser.showSaveDialog(frame);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    System.out.println("Save as file: " + fileToSave.getAbsolutePath());

                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        List<ProviderWithExportData> exportProviders = new ArrayList<>();
                        for (Provider provider : customListener.providers) {
                            exportProviders.add(new ProviderWithExportData(provider));
                        }
                        mapper.writeValue(new File(fileToSave.getAbsolutePath() + ".json"), exportProviders);
                    } catch (Exception error) {
                        error.printStackTrace();
                    }

                }
            }

        });

        for (ProviderTypes value : ProviderTypes.values()) {
            leftMenu.providerTypeSelection.addItem(value);
        }

        leftMenu.providerTypeSelection.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                customListener.selectedProvider.setConnectionType((ProviderTypes) leftMenu.providerTypeSelection.getSelectedItem());
            }
        });

        // Add a selection painter
        SelectionAdapter sa = new SelectionAdapter(mapViewer);
        SelectionPainter sp = new SelectionPainter(sa);
        mapViewer.addMouseListener(sa);
        mapViewer.addMouseMotionListener(sa);
        mapViewer.setOverlayPainter(sp);

        mapViewer.addPropertyChangeListener("zoom", new PropertyChangeListener()
        {
            @Override
            public void propertyChange(PropertyChangeEvent evt)
            {
                updateWindowTitle(frame, mapViewer);
            }
        });

        mapViewer.addPropertyChangeListener("center", new PropertyChangeListener()
        {
            @Override
            public void propertyChange(PropertyChangeEvent evt)
            {
                updateWindowTitle(frame, mapViewer);
            }
        });

        updateWindowTitle(frame, mapViewer);


    }

    protected static void updateWindowTitle(JFrame frame, JXMapViewer mapViewer)
    {
        double lat = mapViewer.getCenterPosition().getLatitude();
        double lon = mapViewer.getCenterPosition().getLongitude();
        int zoom = mapViewer.getZoom();

        frame.setTitle(String.format("Visualization (%.2f / %.2f) - Zoom: %d", lat, lon, zoom));
    }
}
