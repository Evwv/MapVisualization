import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProviderWithExportData {
    public Provider provider;
    public List<List<Integer>> positionMatrix;

    public ProviderWithExportData(Provider provider) {
        this.provider = provider;
        transformPositionsToMatrix();
    }

    private void transformPositionsToMatrix() {
        positionMatrix = new ArrayList<>();
        for (MyGeoPosition firstGeoPosition : provider.getAllGeoPositions()) {
            List<Integer> row = new ArrayList<>();
            for (MyGeoPosition secondGeoPosition : provider.getAllGeoPositions()) {
                row.add(provider.isRouteAlreadyExists(Arrays.asList(firstGeoPosition, secondGeoPosition)) ? 1 : 0);
            }
            positionMatrix.add(row);
        }
    }
}
